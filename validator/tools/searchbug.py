import re
from functools import wraps
import urllib.parse


def unknown_by_error(f):
    @wraps(f)
    def wrapper(*pargs, **kwargs):
        try:
            res = f(*pargs, **kwargs)
            return res
        except TypeError:
            return "unknown"
    return wrapper


class SbParser:
    def __init__(self, response):
        self.xpath = response.xpath
        self.response = response

    @property
    @unknown_by_error
    def phone_type(self):
        type_xp = "//div[contains(text(), 'Phone Type:')]/\
following-sibling::div/text()"
        phone_type = self.xpath(type_xp).extract_first()
        phone_type = re.sub(r'\([\w\d ]+\)', '', phone_type)
        phone_type = phone_type.strip()
        phone_type = phone_type.lower()
        return phone_type

    @property
    def phone_number(self):
        phone = self.response.meta['phone']
        phone = urllib.parse.unquote(phone)
        return phone

    @property
    @unknown_by_error
    def carrier(self):
        carrier_xp = "//div[contains(text(), 'Carrier:')]/\
following-sibling::div/text()"
        carrier = self.xpath(carrier_xp).extract_first()
        carrier = re.sub(r'\([\w\d ]+\)', '', carrier)
        carrier = re.sub(r'\*', '', carrier)
        carrier = carrier.strip()
        carrier = carrier.lower()
        return carrier

    @property
    def is_valid(self):
        xp = "//h1/text()"
        text = self.xpath(xp).extract_first()
        text = text.lower()

        if "is valid" in text:
            return True

        return False
