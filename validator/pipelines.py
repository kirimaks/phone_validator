# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class ValidatorPipeline(object):

    def process_item(self, item, spider):
        with spider.db_conn.cursor() as cursor:
            if item['is_valid']:
                query = """UPDATE phones SET type = %s, carrier = %s
                           WHERE phone = %s"""

                cursor.execute(query, [item['type'], item['carrier'], item['phone']])

            else:
                query = """UPDATE phones SET type = 'error'
                           WHERE phone = %s"""
                cursor.execute(query, [item['phone']])

            spider.db_conn.commit()
           
        return item
