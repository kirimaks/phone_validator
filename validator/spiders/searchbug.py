# -*- coding: utf-8 -*-
import scrapy
import psycopg2
import urllib.parse
from scrapy.http import Request
from validator.tools import searchbug


class SearchbugSpider(scrapy.Spider):
    name = "searchbug"
    allowed_domains = ["searchbug.com"]
    base_url = "https://www.searchbug.com/tools/\
landline-or-cellphone.aspx?FULLPHONE={}"

    def __init__(self, *pargs, **kwargs):
        super(SearchbugSpider, self).__init__(*pargs, **kwargs)
        self.db_conn = psycopg2.connect("host=database user=ubuntu\
 dbname=scraped_data")

    def start_requests(self):
        with self.db_conn.cursor() as cursor:
            query = """SELECT phone FROM phones
                       WHERE type is NULL"""
            cursor.execute(query)
            for phone in cursor.fetchall():
                phone = urllib.parse.quote(phone[0])
                yield Request(self.base_url.format(phone),
                              callback=self.parse_result,
                              meta={"phone": phone})

    def parse_result(self, response):
        data = dict()
        parser = searchbug.SbParser(response)

        data['is_valid'] = parser.is_valid
        data['type'] = parser.phone_type
        data['phone'] = parser.phone_number
        data['carrier'] = parser.carrier

        yield data
