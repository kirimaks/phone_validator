FROM ubuntu:latest

RUN apt update && apt upgrade -y
RUN apt install -y python3-dev
RUN apt install -y python3-pip
RUN apt install -y libssl-dev
RUN apt install -y libffi-dev
RUN apt install -y git

RUN git clone https://kirimaks@bitbucket.org/kirimaks/phone_validator.git
RUN pip3 install --upgrade pip
RUN pip3 install -r /phone_validator/requirements.txt

CMD /usr/bin/python3 /phone_validator/run.py
