import scrapy
from scrapy.crawler import CrawlerProcess
from validator.spiders import searchbug
import validator.settings

settings = {}
for opt_name in [opt for opt in dir(validator.settings) if not opt.startswith("__")]:
    settings[opt_name] = validator.settings.__getattribute__(opt_name)

process = CrawlerProcess(settings)
process.crawl(searchbug.SearchbugSpider)
process.start()
